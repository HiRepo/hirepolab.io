NTSDK's Native Cross-platform Module  
====================================
Welcome to **Line-Games's cross-platform native Library**, 

## OS Host ##
  * Windows 10 v1903    ( WSL Debian 9.4+ )
  * Mac latest version

## Apply Min Version ##
  * Visual Studio 2019+
  * g+__+  9.1+
  * NDK  r20+
  * Mac OSX 10.14+
  * XCode 10.1+

## Environment Variable ##
  * ANDROID_NDK_HOME

## Tools ##
> You Need to below packages. 
  * [CMake](https://cmake.org/) v3.9.+
  * [NDK](https://github.com/android-ndk/ndk/wiki) linux r17+
     wget https://dl.google.com/android/repository/android-ndk-r17-linux-x86_64.zip?hl=ko
  * Android Studio 3.+
  * Unreal Engine 4.21+
  * Unity 2017.4.16+  

## WSL Setting ##
> First, install debian(wsl) in Windows Store 
  * sudo apt-get update && sudo apt-get upgrade		
  * sudo apt-get -y install  build-essential libzip-dev 
  * sudo apt-get -y install  git cmake curl rsync xmlstarlet jq
    
## MAC Setting ##
> First, need Mac & XCode Update 
  * sudo xcode-select -s 
  * sudo gem install xcpretty
  * brew install coreutils 
  * brew findutils --with-default-names
  * brew install cmake xmlstarlet jq

## Build ##
  * make          ( build Host Platform )
  * make all      ( build All Platform )
  * make window   ( build Window )
  * make android  ( build Android _)
  * make ios      ( build iOS )

## Build with DebugInfo ## 
  * make BUILD___CONFIG=RelWithDebInfo

## Build Output ##
  * .build**/
  * .install/

## Configuration  ##
> if you need to configurate, then you can make config file.
  * touch config.mk  
  * Sample  ( this is default setting ) 
```makefile
_SERVER_MAC = git@10.95.163.???
_PATH_NDK = /mnt/d/Android/android-ndk-r17
_PATH_ANDROID_SDK := /mnt/d/Android/SDK
     _CMAKE_window = /mnt/c/Program\ Files\ (x86)/Microsoft Visual\ Studio/2017/Professional/Common7/IDE/CommonExtensions/Microsoft/CMake/CMake/bin/cmake.exe
ifndef __MAC__
   _COMM_UNITY = /mnt/c/Program\ Files/Unity/Editor/Unity.exe
endif 
```

<br/><br/>
